Source: sdpb
Maintainer: Debian Science Team <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Tobias Hansen <thansen@debian.org>
Section: math
Priority: optional
Standards-Version: 3.9.8
Homepage: https://github.com/davidsd/sdpb
Vcs-Browser: https://salsa.debian.org/science-team/sdpb
Vcs-Git: https://salsa.debian.org/science-team/sdpb.git
Build-Depends: debhelper (>= 10),
               libboost-chrono-dev,
               libboost-filesystem-dev,
               libboost-program-options-dev,
               libboost-serialization-dev,
               libboost-system-dev,
               libboost-timer-dev,
               libgmp-dev,
               libtinyxml2-dev
Build-Depends-Indep: texlive-fonts-recommended,
                     texlive-latex-base,
                     texlive-latex-extra,
                     texlive-latex-recommended,
                     texlive-pictures

Package: sdpb
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Recommends: sdpb-doc
Description: Semidefinite program solver
 SDPB is an open-source, arbitrary-precision, parallelized semidefinite
 program solver, designed for the conformal bootstrap. SDPB significantly
 outperforms less specialized solvers and should enable many new computations.
 .
 For more information, see "A Semidefinite Program Solver for the Conformal
 Bootstrap" at http://arxiv.org/abs/1502.02033.

Package: sdpb-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Description: Semidefinite program solver (documentation)
 SDPB is an open-source, arbitrary-precision, parallelized semidefinite
 program solver, designed for the conformal bootstrap. SDPB significantly
 outperforms less specialized solvers and should enable many new computations.
 .
 For more information, see "A Semidefinite Program Solver for the Conformal
 Bootstrap" at http://arxiv.org/abs/1502.02033.
 .
 This package contains the manual and example files, including a Mathematica
 file with code to export semidefinite programs in the XML-based format used
 by SDPB.
